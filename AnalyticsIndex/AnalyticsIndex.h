#define ALPHABET_LENGTH 26

#include "../IndexItem/IndexItem.h"
#include <cctype>
using namespace TLS;

/**
* class AnalyticsIndex
* @author Gabriele Semeraro <gab.semeraro@gmail.com>
*/
class AnalyticsIndex{
    private:
        List <IndexItem> index[ ALPHABET_LENGTH ]; /**< Array of List type that holds IndexItem objects */

    public:
        /**
         * searchIndex()
         * @brief function that find the position of the index list
         * @param string for searching the index of the array index
         * @throw invalid_argument exception when the argument is not found in the index list
         * @return int
        */
        static int searchIndex(string voice);

        /**
         * createNode()
         * @brief function that find the position of the index of the array index
         * @param IndexItem for insert the object in the array index
         * @throw length_error exception when the property pages is empty
         * @throw invalid_argument exception when the item already exist
        */
        void createNode(IndexItem item);

        /**
         * foundItem()
         * @brief function that find the object in the array index
         * @param string for searching the object in the array index
         * @return IndexItem*
        */
        IndexItem* foundItem(string voice);

        /**
         * foundItem()
         * @brief function that update the property pages of the object
         * @param string for searching the object in the array index
         * @param unsigned int number of page to update
         * @return bool
        */
        bool updateItemPage(string voice, unsigned int page);
        
        /**
         * deleteItem()
         * @brief function that delete the object in the array index
         * @param string for searching the object in the array index
         * @return IndexItem*
        */
        IndexItem* deleteItem(string voice);

        /**
         * showIndex()
         * @brief function that show all the index
        */
        void showIndex();
};