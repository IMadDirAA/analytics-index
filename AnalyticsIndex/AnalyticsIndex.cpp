#include "AnalyticsIndex.h"
#include <vector>

int AnalyticsIndex::searchIndex(string voice){
    //97 is the ascii number of the letter 'a'
    if( IndexItem::isVoiceTrue(voice) )
        return tolower( voice[0] ) - 97;

    throw invalid_argument("ERROR: invalid argument " + voice);
}

void AnalyticsIndex::createNode(IndexItem item){
    int pos = this->searchIndex( item.getVoice() );

    if( !item.pages.length() )
        throw length_error("ERROR: length error pages = 0");

    if(this->index[pos].found(item))
        throw invalid_argument("ERROR: invalid argument IndexItem already exist");

    this->index[pos].insertInOrder(item);
}

IndexItem* AnalyticsIndex::foundItem(string voice){
    IndexItem item(voice);

    return this->index[ this->searchIndex(voice) ].found(item);
}

bool AnalyticsIndex::updateItemPage(string voice, unsigned int page){
    IndexItem item(voice);
    IndexItem* p;

    p = this->index[ this->searchIndex(voice) ].found(item);
    
    if(p){
        if( !p->pages.found(page) )
            p->pages.insertInOrder(page);
    } else{
        return false;
    }

    return true;
}

IndexItem* AnalyticsIndex::deleteItem(string voice){
    IndexItem item(voice);

    return this->index[ this->searchIndex(voice) ].eliminate(item);
}

void AnalyticsIndex::showIndex(){
    int i, j, dim;
    int letter = 65;

    for(i = 0; i < ALPHABET_LENGTH; i++){
        dim = this->index[i].length();

        if(dim){
            printf("%c\n", letter);

            for(j = 0; j < dim; j++){
                cout << "\t" << this->index[i][j].getVoice();
                this->index[i][j].pages.show();
            }
        }

        letter += 1;
    }
}
