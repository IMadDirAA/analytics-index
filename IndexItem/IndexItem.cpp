//
// Created by imadd on 01/12/2019.
//

#include <exception>
#include "IndexItem.h"

/**
 * IndexItem()
 * constructor which initialize IndexItem's propriety
 * @param string voice
 * @throw invalid_argument exception when argument voice is invalid
 */
IndexItem::IndexItem(const std::string &voice) {
    if (!IndexItem::isVoiceTrue(voice))
        throw invalid_argument("ERROR: invalid argument " + voice);
    this->voice = voice;
}

/**
 * IndexItem()
 * constructor which initialize IndexItem's propriety
 * @param const string &voice
 * @param List<unsigned int> pages
 * @throw invalid_argument exception when argument voice is invalid
 */
IndexItem::IndexItem(const std::string& voice, TLS::List<unsigned int> pages) {
    if (!IndexItem::isVoiceTrue(voice))
        throw invalid_argument("ERROR: invalid argument " + voice);
    this->voice = voice;
    this->pages = pages;
}

/**
 * setVoice()
 * @param const string &newVoice
 * @throw invalid_argument exception when argument voice is invalid
 * @return void
 */
void IndexItem::setVoice(const std::string& newVoice) {
    if (!IndexItem::isVoiceTrue(newVoice))
        throw invalid_argument("ERROR: invalid argument " + newVoice);
    this->voice = newVoice;
}

/**
 * getVoice()
 * @return string
 */
std::string IndexItem::getVoice() const noexcept {
    return this->voice;
}

/**
 * isVoiceTrue()
 * the func return true if argument voice is valid
 * @param string voice
 * @return bool
 */
bool IndexItem::isVoiceTrue(std::string voice) noexcept {
    return (voice[0] > 64 and voice[0] < 92) or (voice[0] > 96 and voice[0] < 124);
}
