//
// Created by imadd on 01/12/2019.
//

#ifndef ANALYTICS_INDEX_INDEXITEM_H
#define ANALYTICS_INDEX_INDEXITEM_H

#include <string>
#include "../List/List.h"

/**
 * IndexItem
 * @author Imad Diraa <imad.diraa1092@gmail.com>
 */
class IndexItem {
private:
    /** @var std::string voice */
    std::string voice;
public:
    /** @var std::string voice */
    TLS::List<unsigned int> pages;

    /**
     * IndexItem()
     * constructor which initialize IndexItem's propriety
     * @param const string &voice
     * @throw invalid_argument exception when argument voice is invalid
     */
    explicit IndexItem(const std::string &voice);

    /**
     * IndexItem()
     * constructor which initialize IndexItem's propriety
     * @param string voice
     * @param List<unsigned int> pages
     * @throw invalid_argument exception when argument voice is invalid
     */
    IndexItem(const std::string& voice, TLS::List<unsigned int> pages);

    /**
     * setVoice()
     * @param const string &newVoice
     * @throw invalid_argument exception when argument voice is invalid
     * @return void
     */
    void setVoice(const std::string& newVoice);

    /**
     * getVoice()
     * @return string
     */
    std::string getVoice() const noexcept;

    /**
     * isVoiceTrue()
     * the func return true if argument voice is valid
     * @param string voice
     * @return bool
     */
    static bool isVoiceTrue(std::string voice) noexcept;

    /**
     * operator==()
     * @param IndexItem item
     * @return bool
     */
    bool operator == (const IndexItem &item) {
        return this->voice == item.voice;
    }

    /**
     * operator!=()
     * @param IndexItem item
     * @return bool
     */
    bool operator != (const IndexItem &item) {
        return this->voice != item.voice;
    }

    /**
     * operator<()
     * @param IndexItem item
     * @return bool
     */
    bool operator < (const IndexItem &item) {
        return this->voice < item.voice;
    }

    /**
     * operator>()
     * @param IndexItem item
     * @return bool
     */
    bool operator > (const IndexItem &item) {
        return this->voice > item.voice;
    }

    /**
     * operator<=()
     * @param IndexItem item
     * @return bool
     */
    bool operator <= (const IndexItem &item) {
        return this->voice <= item.voice;
    }

    /**
     * operator>=()
     * @param IndexItem item
     * @return bool
     */
    bool operator >= (const IndexItem &item) {
        return this->voice >= item.voice;
    }
};


#endif //ANALYTICS_INDEX_INDEXITEM_H
