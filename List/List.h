//
// Created by imadd on 27/11/2019.
//

#ifndef DIZIONARIO_List_H
#define DIZIONARIO_List_H

#include "NodoBl.h"
#include <vector>

/**
* @namespace TLS(TOOLS) which include class List, NodoBL, Empty_list
*/
namespace TLS {

    /**
    * class List
    * @author Imad Diraa <imad.diraa1092@gmail.com>
     * @tparam T
    */
    template <class T>
    class List {
    private:
        /** @var NodoBL<T>* root */
        NodoBL<T>* root;
        /** @var NodoBL<T>* end */
        NodoBL<T>* end;
        /** @var NodoBL<T>* temp */
        NodoBL<T>* temp;
        /** @var NodoBL<T>* scroll */
        NodoBL<T>* scroll;
        /** @var NodoBL<T>* pscroll */
        NodoBL<T>** pscroll;
        /** @var int len */
        int len;

    public:
        /**
         * List()
         * constructor by default which initialize List's propriety
         * @return void
         */
        List() noexcept;

        /**
         * pushInRoot()
         * the function push in root of List element T date
         * @param T date which represent generic type
         * @throw bad_alloc exception when the program has insufficient memory to alloc a new NodoBL
         * @return void
         */
        void pushInRoot(T date);

        /**
         * pushInEnd()
         * the function push in end of List element T date
         * @param T date which represent generic type
         * @throw bad_alloc exception when the program has insufficient memory to alloc a new NodoBL
         * @return void
         */
        void pushInEnd(T date);

        /**
         * popInRoot()
         * the function pop the first element from List
         * @throw Empty_List exception when the List is empty
         * @return T poppedElement
         */
        T popInRoot();

        /**
         * popInEnd()
         * the function pop the last element from List
         * @throw Empty_List exception when the List is empty
         * @return T poppedElement
         */
        T popInEnd();

        /**
         * show()
         * the function show the element of List
         * @return void
         */
        void show() const noexcept;

        /**
         * length()
         * the function interrogate the size of List
         * @const true
         * @return int
         */
        int length() const noexcept;

        /**
         * operator[]()
         * the function interrogate the size of List
         * @param const int &p which represent the position of the element in List
         * @throw out_of_range exception when the position is out of range in List
         * @return T&
         */
        T& operator[] (const int &p);

        /**
         * insertIn()
         * the function insert date in List
         * @param T date which represent the element to insert in List
         * @param int p which represent the position of the element in List
         * @throw out_of_range exception when the position is out of range in List
         * @throw bad_alloc exception when the program has insufficient memory to alloc a new NodoBL
         * @return void
         */
        void insertIn(T date, int p);

        /**
         * sort()
         * the function sort the list
         * @return void
         */
        void sort();

        /**
         * sort()
         * the function sort the list
         * @param bool whenIsSmall is a function which decide the order of list
         * @return void
         */
        void sort(bool (*whenIsSmall)(T, T));

        /**
         * insertInOrder()
         * the function insert date into the List in order
         * @param T date which represent the element to insert in List
         * @throw bad_alloc exception when the program has insufficient memory to alloc a new NodoBL
         * @return void
         */
        void insertInOrder(T date);

        /**
         * found()
         * the func search for a date that equal to a date parameter and turn a pointer to it
         * @param T date
         * @return T*
         */
        T* found(T date);

        /**
         * eliminate()
         * the func eliminate a date if they are equal and return a pointer to date or null if there isn't date equal
         * @tparam T which represent the generic type
         * @param T date which represent the element to found
         * @throw bad_alloc exception when the program has insufficient memory to alloc a new NodoBL
         * @return T*
         */
        T* eliminate(T date);

        /**
         * toArray()
         * @return T*
         */
        T* toArray();
    };
}

#include "ExceptionType.h"
#include "List.cpp"

#endif //DIZIONARIO_List_H
