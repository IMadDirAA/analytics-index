//
// Created by imadd on 27/11/2019.
//

#include <string>
#include "ExceptionType.h"

namespace TLS {
    // TLS::EmptyList method define

    /**
     * EmptyList()
     * the unique constructor which initialize EmptyList's proprety
     * @param const std::string &Msg which represent the message error
     * @return void
     */
    EmptyList::EmptyList(const std::string& Msg) noexcept
    {
        this->Msg = Msg;
    }

    /**
     * ~EmptyList()
     * the unique destructor which eliminate EmptyList object
     * @return void
     */
    EmptyList::~EmptyList() noexcept = default;

    /**
    * what()
    * the function return the message error
    * @return const char*
    */
    const char * EmptyList::what () const noexcept
    {
        return Msg.c_str();
    }


    // FormatError method define

    /**
     * FormatError()
     * the unique constructor which initialize FormatError's proprety
     * @param const std::string &Msg which represent the message error
     * @return void
     */
    FormatError::FormatError(const std::string& Msg) noexcept
    {
        this->Msg = Msg;
    }

    /**
     * ~FormatError()
     * the unique destructor which eliminate FormatError object
     * @return void
     */
    FormatError::~FormatError() noexcept = default;

    /**
     * what()
     * the function return the message error
     * @return const char*
     */
    const char * FormatError::what () const noexcept
    {
        return Msg.c_str();
    }
}