//
// Created by imadd on 27/11/2019.
//

#ifndef DIZIONARIO_EXCEPTIONTYPE_H
#define DIZIONARIO_EXCEPTIONTYPE_H


#include <exception>
#include <string>

/**
 @namespace TLS(TOOLS) which include class List, NodoBL, Empty_list
*/
namespace TLS {

    /**
    * class EmptyList
    * @author Imad Diraa <imad.diraa1092@gmail.com>
    */
    class EmptyList:public std::exception
    {
    private:
        /** @var std::string Msg */
        std::string Msg;

    public:
        /**
         * EmptyList()
         * the unique constructor which initialize EmptyList's proprety
         * @param const std::string &Msg which represent the message error
         * @return void
         */
        explicit EmptyList(const std::string& Msg) noexcept;

        /**
         * ~EmptyList()
         * the unique destructor which eliminate EmptyList object
         * @return void
         */
        ~EmptyList() noexcept override;

        /**
         * what()
         * the function return the message error
         * @return const char*
         */
        const char * what () const noexcept override;
    };

    /**
    * class FormatError
    * @author Imad Diraa <imad.diraa1092@gmail.com>
    */
    class FormatError:public std::exception
    {
    private:
        /** @var std::string Msg */
        std::string Msg;

    public:
        /**
         * FormatError()
         * the unique constructor which initialize FormatError's proprety
         * @param const std::string &Msg which represent the message error
         * @return void
         */
        explicit FormatError(const std::string& Msg) noexcept;

        /**
         * ~FormatError()
         * the unique destructor which eliminate FormatError object
         * @return void
         */
        ~FormatError() noexcept override;

        /**
         * what()
         * the function return the message error
         * @return const char*
         */
        const char * what () const noexcept override;
    };
}

#endif //DIZIONARIO_EXCEPTIONTYPE_H
