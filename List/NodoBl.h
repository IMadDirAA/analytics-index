//
// Created by imadd on 27/11/2019.
//

#ifndef DIZIONARIO_NODOBL_H
#define DIZIONARIO_NODOBL_H


#include <iostream>

/**
 @namespace TLS(TOOLS) which include class Lista, NodoBL, Empty_list
*/
namespace TLS {

    /**
    * class NodoBL
    * @author Imad Diraa <imad.diraa1092@gmail.com>
    */
    template <class T>
    class NodoBL {
    public:
        /** @var T date */
        T date;
        /** @var NodoBL<T>* prev */
        NodoBL<T>* prev;
        /** @var NodoBL<T>* next */
        NodoBL<T>* next;

        /**
         * NodoBL()
         * constructor by default which initialize NodoBL's proprety
         * @return void
         */
        NodoBL();

        /**
         * NodoBL()
         * second constructor which initialize NodoBL's proprety and template type
         * @param T date which represent generic type
         * @return void
         */
        explicit NodoBL(T date);
    };
}

#include "NodoBl.cpp"

#endif //DIZIONARIO_NODOBL_H
