//
// Created by imadd on 27/11/2019.
//

#ifndef DIZIONARIO_NODOBL_CPP
#define DIZIONARIO_NODOBL_CPP

#include "NodoBl.h"

/**
 * NodoBL()
 * constructor by default which initialize NodoBL's proprety
 * @tparam class T which represent generic type
 * @return void
 */
template <class T>
TLS::NodoBL<T>::NodoBL()
{
    this->next = NULL;
    this->prev = NULL;
}

/**
 * NodoBL()
 * second constructor which initialize NodoBL's proprety and template type
 * @param T date which represent generic type
 * @tparam class T which represent generic type
 * @return void
 */
template <class T>
TLS::NodoBL<T>::NodoBL(T date): date(date)
{
    this->next = NULL;
    this->prev = NULL;
}


#endif
