//
// Created by imadd on 27/11/2019.
//

#ifndef DIZIONARIO_List_CPP
#define DIZIONARIO_List_CPP

#include "List.h"
#include <stdexcept>
#include <iostream>
#include <malloc.h>
#include <vector>
using namespace std;

/**
 * List()
 * constructor by default which initialize List's propriety
 * @return void
 */
template <class T>
TLS::List<T>::List() noexcept
{
    this->root = nullptr;
    this->end = nullptr;
    this->scroll = nullptr;
    this->pscroll = nullptr;
    this->len = 0;
    this->temp = nullptr;
}

/**
 * pushInRoot()
 * the function push in root of List element T date
 * @param T date which represent generic type
 * @throw bad_alloc exception when the program has insufficient memory to alloc a new NodoBL
 * @return void
 */
template <class T>
void TLS::List<T>::pushInRoot(T date)
{
    this->temp = this->root;
    this->root = new TLS::NodoBL<T>(date);
    this->root->next = this->temp;
    this->root->prev = nullptr;
    if (this->temp)
        this->temp->prev = this->root;
    else
        this->end = root;
    this->len++;
}

/**
 * pushInEnd()
 * the function push in end of List element T date
 * @param T date which represent generic type
 * @throw bad_alloc exception when the program has insufficient memory to alloc a new NodoBL
 * @return void
 */
template <class T>
void TLS::List<T>::pushInEnd(T date)
{
    if (!this->end)
        return pushInRoot(date);

    this->end->next = new NodoBL<T>(date);
    this->end->next->next = nullptr;
    this->end = this->end->next;
    this->end->next = nullptr;
    this->len++;
}

/**
 * popInRoot()
 * the function pop the first element from List
 * @throw Empty_List exception when the List is empty
 * @return T poppedElement
 */
template <class T>
T TLS::List<T>::popInRoot()
{
    T date;
    if (!this->len)
        throw EmptyList("empty List");
    this->temp = this->root;
    this->root = this->root->next;
    this->len--;
    if (!this->len)
        this->end = nullptr;
    if (this->len == 1) {
        this->root->prev = nullptr;
        this->end = this->root;
    }
    date = temp->date;
    delete temp;
    return date;
}

/**
 * popInEnd()
 * the function pop the last element from List
 * @throw Empty_List exception when the List is empty
 * @return T poppedElement
 */
template <class T>
T TLS::List<T>::popInEnd()
{
    T date;
    if (!this->len)
        throw EmptyList("empty List");
    this->temp = this->end;
    this->end = this->end->prev;
    this->end->next = nullptr;
    this->len--;
    if (!this->len)
        this->root = nullptr;
    date = this->temp->date;
    delete temp;
    return date;
}

/**
 * show()
 * the function show the element of List
 * @return void
 */
template <class T>
void TLS::List<T>::show() const noexcept
{
    cout<<endl<<"\t(";
    for (NodoBL<T>* t = root; t; t = t->next) {
        cout<<t->date;
        if (t->next)
            cout<<", ";
        t->date = t->date;
    }
    cout<<")"<<endl;
}

/**
 * length()
 * the function interrogate the size of List
 * @const true
 * @return int
 */
template <class T>
int TLS::List<T>::length() const noexcept
{
    return this->len;
}

/**
 * operator[]()
 * the function interrogate the size of List
 * @param const int &p which represent the position of the element in List
 * @throw out_of_range exception when the position is out of range in List
 * @return T&
 */
template <class T>
T& TLS::List<T>::operator[] (const int &p)
{
    if (p < 0 || p >= this->len)
        throw out_of_range("out of range");

    int i = 0;
    for (this->scroll = root; i < p; i++, this->scroll = this->scroll->next);
    return this->scroll->date;
}

/**
 * insertIn()
 * the function insert date in List
 * @tparam T which represent the generic type
 * @param T date which represent the element to insert in List
 * @param int p which represent the position of the element in List
 * @throw out_of_range exception when the position is out of range in List
 * @throw bad_alloc exception when the program has insufficient memory to alloc a new NodoBL
 * @return void
 */
template <class T>
void TLS::List<T>::insertIn(T date, int p)
{
    if (p < 0 || p > this->len)
        throw out_of_range("out of range");

    this->pscroll = &this->root;
    for (int i = 0; i < p; i++, this->pscroll = &(*(this->pscroll))->next);

    this->temp = *(this->pscroll);
    *(this->pscroll) = new NodoBL<T>(date);
    (*(this->pscroll))->next = this->temp;
    if (this->temp)
        (*(this->pscroll))->prev = this->temp->prev;
    else
        this->end = *(this->pscroll);
    this->len++;
}

 /**
  * sort()
  * the function sort the list
  * @return void
  */
template <class T>
void TLS::List<T>::sort()
{
    // check if that are element can be sorted
    if (this->len < 2)
        return;

    /**
     * @var NodoBL<T>** element
     */
    NodoBL<T>** element = nullptr;
    NodoBL<T>** head = nullptr;

    // scroll all element
    for (head = &(this->root); (*head)->next; head = &(*head)->next) {
        this->pscroll = &(*head)->next;
        element = head;

        // check the small element
        for (; (*(this->pscroll)) != nullptr; (this->pscroll) = &(*(this->pscroll))->next)
            if ((*element)->date > (*(this->pscroll))->date)
                element = (this->pscroll);

        // if there is a small element
        if (element != head) {
            this->temp = *element;//1
            *element = (*element)->next;//2
            if (*element)
                (*element)->prev = this->temp->prev;

            this->temp->next =*head;//4
            this->temp->prev = (*head)->prev;//5
            *head = this->temp;//6
        }
    }
    this->end = *head;
}

/**
* sort()
* the function sort the list
* @param bool whenIsSmall is a function which decide the order of list
* @return void
*/
template<class T>
void TLS::List<T>::sort(bool (*whenIsSmall)(T, T)) {
    // check if that are element can be sorted
    if (this->len < 2)
        return;

    /**
     * @var NodoBL<T>** element
     * @var NodoBL<T>** head
     */
    NodoBL<T>** element = nullptr;
    NodoBL<T>** head = nullptr;

    // scroll all element
    for (head = &(this->root); (*head)->next; head = &(*head)->next) {
        this->pscroll = &(*head)->next;
        element = head;

        // check the small element
        for (; (*(this->pscroll)) != nullptr; (this->pscroll) = &(*(this->pscroll))->next)
            if (!whenIsSmall((*element)->date , (*(this->pscroll))->date))
                element = (this->pscroll);

        // if there is a small element
        if (element != head) {
            this->temp = *element;//1
            *element = (*element)->next;//2
            if (*element)
                (*element)->prev = this->temp->prev;

            this->temp->next =*head;//4
            this->temp->prev = (*head)->prev;//5
            *head = this->temp;//6
        }
    }
    this->end = *head;
}

/**
 * insertInOrder()
 * the function insert date into the List in order
 * @tparam T which represent the generic type
 * @param T date which represent the element to insert in List
 * @throw bad_alloc exception when the program has insufficient memory to alloc a new NodoBL
 * @return void
 */
template<class T>
void TLS::List<T>::insertInOrder(T date)
{
    for (this->pscroll = &this->root; *this->pscroll && (*(this->pscroll))->date < date;)
        this->pscroll = &(*(this->pscroll))->next;

    this->temp = *(this->pscroll);
    *(this->pscroll) = new NodoBL<T>(date);
    (*(this->pscroll))->next = this->temp;
    if (this->temp) {
        (*(this->pscroll))->prev = this->temp->prev;
        this->temp->prev = *(this->pscroll);
    }
    else
        this->end = *(this->pscroll);
    this->len++;
}

/**
 * found()
 * the func search for a date that equal to a date parameter and turn a pointer to it
 * @param T date
 * @return T*
 */
template<class T>
T* TLS::List<T>::found(T date)
{
    for (this->scroll = this->root; this->scroll && this->scroll->date != date; this->scroll = this->scroll->next);
    if (this->scroll)
        return &(this->scroll->date);
    return nullptr;
}

/**
 * eliminate()
 * the func eliminate a date if they are equal and return a pointer to date or null if there isn't date equal
 * @tparam T which represent the generic type
 * @param T date which represent the element to found
 * @throw bad_alloc exception when the program has insufficient memory to alloc a new NodoBL
 * @return T*
 */
template<class T>
T *TLS::List<T>::eliminate(T date) {

    /**
     * @var T* eliminatedDate
     */
    T* eliminatedDate = (T*)malloc(sizeof(T));

    // search for date
    for (this->pscroll = &this->root; *this->pscroll && (*(this->pscroll))->date != date;)
        this->pscroll = &(*(this->pscroll))->next;

    // if there is a date are equal
    if (*(this->pscroll)) {
        // save
        this->temp = *(this->pscroll);

        // eliminate it from the List
        *(this->pscroll) = this->temp->next;

        // update end if necessary
        if (!this->temp->next)
            this->end = this->temp->prev;
        // update prev if necessary
        else
            this->temp->next->prev = this->temp->prev;
        // save the date
        *eliminatedDate = this->temp->date;
        // and free the heap memory
        free(this->temp);
        return eliminatedDate;
    }
    return nullptr;
}

template<class T>
T* TLS::List<T>::toArray() {
    T* dates = (T*) malloc(sizeof(this->len));

    int i = 0;
    for (this->scroll = root; this->scroll; i++, this->scroll = this->scroll->next)
        dates[i] = this->scroll->date;

    return dates;
}

#endif //DIZIONARIO_List_CPP
